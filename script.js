console.log("JS Objects")

//Objects

let gradesArray = [98, 94, 89, 90];

let grades = {
	firstGrading:98,
	secondGrading:94,
	thirdGrading:89,
	fourthGrading:90,
};
console.log(grades);

let user = {
	firstName: "John",
	lastName: "Doe",
	age:25,
	location: {
		city: "Tokyo",
		country: "Japan"
	},
	emails:["john@mail.com", "johndoe@mail.com"],
	fullName: function(){
		return this.firstName + " " + this.lastName;
	}
};


//creating objects
// A cellphone is an exampel of a real world object. It has its own properties such as name, color, weight unit model, etc.

let cellphone = {
	name: "Nokia 3210",
	manufacturerDate: 1999,
};

console.log("Result From using initializers")
console.log(cellphone);
console.log(typeof cellphone);

//constructor function

function Laptop(name, manufacturerDate){
	this.name = name;
	this.manufacturerDate = manufacturerDate
};

let laptop = new Laptop("Lenovo", 2008);
console.log("Result from using object constructors:");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020)
console.log("Result from using object constructors: ");
console.log(myLaptop);
// what happens when you do not add new keyword when using constructor function
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result without the \"new\" keyword");
console.log(oldLaptop)

//creating empty objeect
let computer = {};
let myComputer = new Object();

//Accessing Object Properties
// dot notation 
console.log("Result from dot notation: " + myLaptop.name);
// square bracket notation
console.log("Result from square bracket: " + myLaptop["name"]);

let array = [laptop, myLaptop];
console.log(array[0]["name"]);
console.log(array[0].name);//best practices

//INitializing object properties, cont. from empty objects

let car = {};

car.name = "Honda Civi";
console.log("Result from adding dot notation");
console.log(car);

//please do not ever do this 
car["manufacture date"] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log("Results from using the square bracket notation");
console.log(car);

//deleting object properties
delete car['manufacture date'];
console.log("Result from deleting properties: ");
console.log(car);

//reassigning properties
car.name = "dodge charger R/T";
console.log("Results from reassigning properties");
console.log(car);

//Objects methods

let person = {
	name: "John",
	talk: function(){
		console.log("Hello my name is " + this.name);
	}
};
console.log(person);
console.log("result from object method")
person.talk()

person.walk = function(){
	console.log(this.name + " walked 25 steps forward.")
}

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address:{
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	emails:["joe@mail.com", "joesmith@mail.com"],
	introduce: function(){
		console.log("Hello my Name is " + this.firstName + " and I live in " + this.address.city)
	},
	receiver: function(){
		console.log("This is my address "+ this.address.city + "," + this.address.state)
	}
}

friend.introduce();
friend.receiver();

//Using object literals to create multiple ikinds of pokemon would be time consuming

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon")
	},
	faint: function(){
		console.log(this.name + " has fainted.")
	}
}

console.log(myPokemon);

// usage of constructor

function MyPokemon(name, level){
	
	//properties
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	//methods
	this.tackle = function(target){
		console.log(this.name + " tackled" + target.name);
		console.log(target.name + "'s health is now reduced to _targetPokemonHealth_");

	};
	this.faint = function(){
		console.log(this.name + "fainted.")
	}
}

let pikachu = new MyPokemon("Pikachu", 16);
let rattata = new MyPokemon(" Rattata", 8);

pikachu.tackle(rattata);